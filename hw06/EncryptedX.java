/*Daniel Karkhut CSE 002 23 October 2018
 * This program asks the user for an integer
 * between 1 and 100 and checks the input. 
 * Then, that number is taken to print the 
 * user's number of columns and rows, creating 
 * an x made out of spaces using nested for loops
 * and an if else statement.
 */

import java.util.Scanner;

public class EncryptedX {
	
	public static void main(String[] args) {
	
	Scanner scan = new Scanner(System.in); //create a scanner
		
		System.out.println("Enter an integer between 1 and 100");
		int i = 0; //initiate variable for size of X
	
		while (scan.hasNext()) { //runs while their is an input given by user
		  if (scan.hasNextInt()) { //checks whether the input is an integer
	    	 i = scan.nextInt(); //sets input value to variable
	    	 	if (i <= 100 && i >= 0) { //checks whether user input number between 1 and 10
	    	 		break;
	    	 	}else {
	    	 	  System.out.println("This is not the correct input, please try again."); //prompts user for another input
	    	 	}
	      } else { //runs when input is not an integer
	    	  System.out.println("This is not the correct input, please try again."); //prompts user for another input
	    	  scan.next(); //erases current input and goes back to the beginning of while loop
	      }
	}
		for(int k = 0;k <= i;k++) { //decides how many rows are create
			for(int j = 0; j <=i;j++) { //decides how many columns
				if(j == k || (i - k) == j) { //places a space starting at 0 from the left and right, incrementing by 1
					System.out.print(" "); //places a space
				} else {
					System.out.print("*"); //places a star
				}
			}
			System.out.println(""); //increments rows
		}
		
	
	scan.close(); //scanner close
} //main
} //class