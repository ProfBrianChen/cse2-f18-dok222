public class lab09 {

    public static int[] copy(int [] array1) {
        int array2[] = new int[array1.length];

        for(int i = 0; i < array1.length; i++) {
            array2[i] = array1[i];
        }

        return array2;
    }

    public static int[] inverter(int [] array1)  {

        for(int i = 0; i < array1.length/2; i++) {
            int j = array1[i];
            array1[i] = array1[(array1.length-1) - i];
            array1[(array1.length-1) - i] = j;
        }

        return null;
    }

    public static int[] inverter2(int [] array1) {
        int array2[] = copy(array1);

        for(int i = 0; i < array1.length; i++) {
            array2[(array1.length-1) - i] = array1[i];
        }
        return array2;
    }

    public static int print(int [] array1) {
        int j = 0;
        for(int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + " ");
        }
        System.out.println("");
        return j;
    }


    public static void main(String[] args) {
        int [] arrayOriginal = new int[8];
        for(int i = 0; i < arrayOriginal.length; i++) {
            arrayOriginal[i] = i;
        }
        int [] array0 = copy(arrayOriginal);
        int [] array1 = copy(arrayOriginal);
        int [] array2 = copy(arrayOriginal);
        int [] array3 ;
        inverter(array0);
        print(array0);
        System.out.println("this is array 0");

        inverter2(array1);
        print(array1);
        System.out.println("this is array 1");

        array3 = inverter2(array2);
        print(array3);
        System.out.println("this is array 3");
    }
}
