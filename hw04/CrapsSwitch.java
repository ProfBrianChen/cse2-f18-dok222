//Daniel Karkhut CSE 002 HW 4
/* This is a program that uses switch statements to pick two 
/seperate dice rolls (a number between 1 and 6) through either
/user unput or random rolls. These are then used to assign a 
/craps name to the certain combnination.
*/
import java.lang.Math;
import java.util.Scanner;

public class CrapsSwitch {
  
  public static void main(String[] args) {
    Scanner myScanner = new Scanner (System.in);
    
    int firstDice = (int) (Math.random() * ((6 - 1) + 1)) + 1; //declares first dice, gives number between 1 and 6 inclusively
    int secondDice = (int) (Math.random() * ((6 - 1) + 1)) + 1; //declares second dice, gives number between 1 and 6 inclusively
    String commonNames = "";
    System.out.println("Press 1 to input dice rolls or press 2 for random dice rolls");
    int decideInput = myScanner.nextInt(); //take input to decide if dice rolls are input or randomized

    switch (decideInput) { //switch that decides whether dice are picked or randomized
      case 1: //dice are input
        System.out.println("First dice roll?");
        firstDice = myScanner.nextInt(); //first dice is picked by user
        
        System.out.println("Second dice roll?");
        secondDice = myScanner.nextInt(); //second dice is picked by user
          switch (firstDice) { //starts cases for first dice from 1 to 6
            case 1:
              switch (secondDice) { //if first dice is 1, second dice is checked, assiging commonNames to the name of the combination
                case 1:
                  commonNames = "Snake Eye";
                  break;  
                case 2:
                  commonNames = "Ace Duece";
                  break;  
                case 3:
                  commonNames = "Easy Four";
                  break;
                case 4:
                  commonNames = "Fever Five";
                  break;
                case 5:
                  commonNames = "Easy Six";
                  break;
                case 6:
                  commonNames = "Seven out";
                  break;
                  
                default:
              System.out.println("You have not entered a number between 1 and 6"); //if second dice isn't between 1 and 6, the user is warned
              }
            break; 
              
            case 2:
              switch (secondDice) { //if first dice is 2, second dice is checked, assiging commonNames to the name of the combination
                case 1:
                  commonNames = "Ace Duece";
                  break;  
                case 2:
                  commonNames = "Hard four";
                  break;  
                case 3:
                  commonNames = "Fever Five";
                  break;
                case 4:
                  commonNames = "Easy six";
                  break;
                case 5:
                  commonNames = "Seven out";
                  break;
                case 6:
                  commonNames = "Easy Eight";
                  break;
                  
                default:
              System.out.println("You have not entered a number between 1 and 6"); //if second dice isn't between 1 and 6, the user is warned
              }
            break;
            
            case 3:
              switch (secondDice) { //if first dice is 3, second dice is checked, assiging commonNames to the name of the combination
                case 1:
                  commonNames = "Easy Four";
                  break;  
                case 2:
                  commonNames = "Fever five";
                  break;  
                case 3:
                  commonNames = "Hard six";
                  break;
                case 4:
                  commonNames = "Seven out";
                  break;
                case 5:
                  commonNames = "Easy Eight";
                  break;
                case 6:
                  commonNames = "Nine";
                  break;
                  
                default:
              System.out.println("You have not entered a number between 1 and 6"); //if second dice isn't between 1 and 6, the user is warned
              }
            break; 
            
            case 4:
              switch (secondDice) { //if first dice is 4, second dice is checked, assiging commonNames to the name of the combination
                case 1:
                  commonNames = "Fever Five";
                  break;  
                case 2:
                  commonNames = "Easy six";
                  break;  
                case 3:
                  commonNames = "Seven out";
                  break;
                case 4:
                  commonNames = "Hard Eight";
                  break;
                case 5:
                  commonNames = "Nine";
                  break;
                case 6:
                  commonNames = "Easy Ten";
                  break;
                  
                default:
              System.out.println("You have not entered a number between 1 and 6"); //if second dice isn't between 1 and 6, the user is warned
              }
            break;
              
            case 5:
              switch (secondDice) { //if first dice is 5, second dice is checked, assiging commonNames to the name of the combination
                case 1:
                  commonNames = "Easy Six";
                  break;  
                case 2:
                  commonNames = "Seven out";
                  break;  
                case 3:
                  commonNames = "Easy Eight";
                  break;
                case 4:
                  commonNames = "Nine";
                  break;
                case 5:
                  commonNames = "Hard Ten";
                  break;
                case 6:
                  commonNames = "Yo-leven";
                  break;
                default:
              System.out.println("You have not entered a number between 1 and 6"); //if second dice isn't between 1 and 6, the user is warned
              }
            break;
              
            case 6:
              switch (secondDice) {//if first dice is 6, second dice is checked, assiging commonNames to the name of the combination
                case 1:
                  commonNames = "Seven out";
                  break;  
                case 2:
                  commonNames = "Easy Eight";
                  break;  
                case 3:
                  commonNames = "Nine";
                  break;
                case 4:
                  commonNames = "Easy Ten";
                  break;
                case 5:
                  commonNames = "Yo-leven";
                  break;
                case 6:
                  commonNames = "Boxcars";
                  break;
                default:
              System.out.println("You have not entered a number between 1 and 6"); //if second dice isn't between 1 and 6, the user is warned
              }
            break;
              
            default:
              System.out.println("You have not entered a number between 1 and 6"); //if first dice isn't between 1 and 6, the user is warned
          }
        break; //end of case 1
        
      case 2: //This case occurs when the dice is chosen to be randomized, same code is used from case 1
        switch (firstDice) {
            case 1:
              switch (secondDice) {
                case 1:
                  commonNames = "Snake Eye";
                  break;  
                case 2:
                  commonNames = "Ace Duece";
                  break;  
                case 3:
                  commonNames = "Easy Four";
                  break;
                case 4:
                  commonNames = "Fever Five";
                  break;
                case 5:
                  commonNames = "Easy Six";
                  break;
                case 6:
                  commonNames = "Seven out";
                  break;
              }
            break; 
              
            case 2:
              switch (secondDice) {
                case 1:
                  commonNames = "Ace Duece";
                  break;  
                case 2:
                  commonNames = "Hard four";
                  break;  
                case 3:
                  commonNames = "Fever Five";
                  break;
                case 4:
                  commonNames = "Easy six";
                  break;
                case 5:
                  commonNames = "Seven out";
                  break;
                case 6:
                  commonNames = "Easy Eight";
                  break;
              }
            break;
            
            case 3:
              switch (secondDice) {
                case 1:
                  commonNames = "Easy Four";
                  break;  
                case 2:
                  commonNames = "Fever five";
                  break;  
                case 3:
                  commonNames = "Hard six";
                  break;
                case 4:
                  commonNames = "Seven out";
                  break;
                case 5:
                  commonNames = "Easy Eight";
                  break;
                case 6:
                  commonNames = "Nine";
                  break;
              }
            break; 
            
            case 4:
              switch (secondDice) {
                case 1:
                  commonNames = "Fever Five";
                  break;  
                case 2:
                  commonNames = "Easy six";
                  break;  
                case 3:
                  commonNames = "Seven out";
                  break;
                case 4:
                  commonNames = "Hard Eight";
                  break;
                case 5:
                  commonNames = "Nine";
                  break;
                case 6:
                  commonNames = "Easy Ten";
                  break;
              }
            break;
              
            case 5:
              switch (secondDice) {
                case 1:
                  commonNames = "Easy Six";
                  break;  
                case 2:
                  commonNames = "Seven out";
                  break;  
                case 3:
                  commonNames = "Easy Eight";
                  break;
                case 4:
                  commonNames = "Nine";
                  break;
                case 5:
                  commonNames = "Hard Ten";
                  break;
                case 6:
                  commonNames = "Yo-leven";
                  break;
              }
            break;
              
            case 6:
              switch (secondDice) {
                case 1:
                  commonNames = "Seven out";
                  break;  
                case 2:
                  commonNames = "Easy Eight";
                  break;  
                case 3:
                  commonNames = "Nine";
                  break;
                case 4:
                  commonNames = "Easy Ten";
                  break;
                case 5:
                  commonNames = "Yo-leven";
                  break;
                case 6:
                  commonNames = "Boxcars";
                  break;
              }
            break; //end of case 2 code
          }
        break; //end of initial switch statement
    }
      
    System.out.println("The first dice is " + firstDice); //prints first dice
    System.out.println("The second dice is " + secondDice); //prints second dice
    System.out.println("You just got a " + commonNames); //prints the name of the combinations
  }
}