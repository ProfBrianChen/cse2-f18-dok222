//Daniel Karkhut CSE 002 HW 4
/* This is a program that uses If statements to pick two 
/seperate dice rolls (a number between 1 and 6) through either
/user unput or random rolls. These are then used to assign a 
/craps name to the certain combnination.
*/
import java.lang.Math;
import java.util.Scanner;

public class CrapsIf {
  
  public static void main(String[] args) {
    Scanner myScanner = new Scanner (System.in);
    
    int firstDice = (int) (Math.random() * ((6 - 1) + 1)) + 1; //declares first dice, gives number between 1 and 6 inclusively
    int secondDice = (int) (Math.random() * ((6 - 1) + 1)) + 1; //declares second dice, gives number between 1 and 6 inclusively
    System.out.println("Press 1 to input dice rolls or press 2 for random dice rolls");
    int decideInput = myScanner.nextInt(); //take input to decide if dice rolls are input or randomized
    
    if (decideInput == 1) { //prepares to accept input
      
      System.out.println("First dice roll?");
      firstDice = myScanner.nextInt(); //first input set to first dice
      
      if (!(firstDice >= 1 && firstDice <= 6)) { //checks if input value is between 1 and 6
        System.out.println("This value is not between 1 and 6, please choose again: ");
        firstDice = myScanner.nextInt(); //sets first dice to second scanner
      }
      
      System.out.println("Second dice roll?");
      secondDice = myScanner.nextInt(); //second input set to second dice
      
      if (!(secondDice >= 1 && secondDice <= 6)) { //checks if input value is between 1 and 6
        System.out.println("This value is not between 1 and 6, please choose again: ");
        secondDice = myScanner.nextInt(); //sets second dice to second scanner 
        
      }
    }
    if (firstDice == 1) { //asks if the first dice is 1, then goes through every possible second dice
      if (secondDice == 1) { //checks for second dice to equal 1
        System.out.println("This pair is Snake Eyes"); //prints first possible solution
      }else if (secondDice == 2) { //checks for second dice to equal 2
        System.out.println("This pair is Ace Duece"); //prints second possible solution
      }else if (secondDice == 3) {
        System.out.println("This pair is Easy four");
      }else if (secondDice == 4) {
        System.out.println("This pair is Fever Five");
      }else if (secondDice == 5) {
        System.out.println("This pair is Easy Six");
      }else if (secondDice == 6) {
        System.out.println("This pair is Seven out");
      }
    }
    if (firstDice == 2) { //asks if the first dice is 2, then goes through every possible second dice
      if (secondDice == 1) {
        System.out.println("This pair is Ace Duece");
      }else if (secondDice == 2) {
        System.out.println("This pair is Hard four");
      }else if (secondDice == 3) {
        System.out.println("This pair is Fever Five");
      }else if (secondDice == 4) {
        System.out.println("This pair is Easy six");
      }else if (secondDice == 5) {
        System.out.println("This pair is Seven out");
      }else if (secondDice == 6) {
        System.out.println("This pair is Easy Eight");
      }
    }  
    if (firstDice == 3) { //asks if the first dice is 3, then goes through every possible second dice
      if (secondDice == 1) {
        System.out.println("This pair is Easy four");
      }else if (secondDice == 2) {
        System.out.println("This pair is Fever Five");
      }else if (secondDice == 3) {
        System.out.println("This pair is Hard six");
      }else if (secondDice == 4) {
        System.out.println("This pair is Seven out");
      }else if (secondDice == 5) {
        System.out.println("This pair is Easy Eight");
      }else if (secondDice == 6) {
        System.out.println("This pair is Nine");
      }
    }
    if (firstDice == 4) { //asks if the first dice is 4, then goes through every possible second dice
      if (secondDice == 1) {
        System.out.println("This pair is Fever Five");
      }else if (secondDice == 2) {
        System.out.println("This pair is Easy six");
      }else if (secondDice == 3) {
        System.out.println("This pair is Seven Out");
      }else if (secondDice == 4) {
        System.out.println("This pair is Hard Eight");
      }else if (secondDice == 5) {
        System.out.println("This pair is Nine");
      }else if (secondDice == 6) {
        System.out.println("This pair is Easy Ten");
      }
    }
    if (firstDice == 5) { //asks if the first dice is 5, then goes through every possible second dice
      if (secondDice == 1) {
        System.out.println("This pair is Easy six");
      }else if (secondDice == 2) {
        System.out.println("This pair is Seven Out");
      }else if (secondDice == 3) {
        System.out.println("This pair is Easy Eight");
      }else if (secondDice == 4) {
        System.out.println("This pair is Nine");
      }else if (secondDice == 5) {
        System.out.println("This pair is Hard Ten");
      }else if (secondDice == 6) {
        System.out.println("This pair is Yo-leven");
      }
    } 
    if (firstDice == 6) { //asks if the first dice is 6, then goes through every possible second dice
      if (secondDice == 1) {
        System.out.println("This pair is Seven Out");
      }else if (secondDice == 2) {
        System.out.println("This pair is Easy Eight");
      }else if (secondDice == 3) {
        System.out.println("This pair is Nine");
      }else if (secondDice == 4) {
        System.out.println("This pair is Easy ten");
      }else if (secondDice == 5) {
        System.out.println("This pair is Yo-leven");
      }else if (secondDice == 6) {
        System.out.println("This pair is Boxcars");
      }
    }  
    System.out.println("The first dice is " + firstDice);
    System.out.println("The second dice is " + secondDice);
  }
}