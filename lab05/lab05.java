/*
 * Daniel Karkhut CSE002 Lab05
 * This code takes inputs from the user using
 * a scanner for various pieces of data on a
 * university course. Using while loops and if-else
 * statements, the inputs are checked to see if they 
 * are input with the correct type of data eg. string,
 * integer, and double. Lastly, the code inputs the data into
 * variables and then prints it to the screen.
 */

import java.util.Scanner; //importing scanner utility

public class lab05 {
	
	public static void main(String[] args) {
	
	Scanner scan = new Scanner(System.in); //creating a scanner
	
	int courseNumber = 0; //course number
	String departmentName = ""; //name of department
	int numberOfVisits = 0; //number of visits
	double timeStart = 0; //starting time of class
	String instructorName = ""; //name of instructor
	int numberOfStudents = 0; //number of students enrolled in class


    System.out.println("What is the course number?");

    while (scan.hasNext()) { //runs while their is an input given by user
      if (scan.hasNextInt()) { //checks whether the input is an integer
    	 courseNumber = scan.nextInt(); //sets input value to variable
         break; //break out of while loop to continue code outside of while loop
      } else { //runs when input is not an integar
    	  System.out.println("This is not the correct input, please try again."); //prompts user for another input
    	  scan.next(); //erases current input and goes back to the beginning of while loop
      }
    }

    //////////
    
    System.out.println("What is the department name?");

    while (scan.hasNext()) {
      if (scan.hasNext()) { //checks whether there is an input
    	 departmentName = scan.next();
         break;
      } else {
    	  System.out.println("This is not the correct input, please try again.");
    	  scan.next();
      }
    }

    //////////
    
    System.out.println("How many times have you visited?");

    while (scan.hasNext()) {
      if (scan.hasNextInt()) {
    	 numberOfVisits = scan.nextInt();
         break;
      } else {
    	  System.out.println("This is not the correct input, please try again.");
    	  scan.next();
      }
    }

    //////////
    
    System.out.println("At what time does the class start? Enter as 00.00");

    while (scan.hasNext()) {
      if (scan.hasNextDouble()) {
    	 timeStart = scan.nextDouble();
         break;
      } else {
    	  System.out.println("This is not the correct input, please try again.");
    	  scan.next();
      }
    }

    ///////////
    
    System.out.println("What is the name of your instructor?");

    while (scan.hasNext()) {
      if (scan.hasNext()) {
    	 instructorName = scan.next();
         break;
      } else {
    	  System.out.println("This is not the correct input, please try again.");
    	  scan.next();
      }
    }
    
    //////////
    
    System.out.println("How many students are in the class?");

    while (scan.hasNext()) {
      if (scan.hasNextInt()) {
    	 numberOfStudents = scan.nextInt();
         break;
      } else {
    	  System.out.println("This is not the correct input, please try again.");
    	  scan.next();
      }
    }

    System.out.println("The course number is: " + courseNumber); 
    System.out.println("The department name is: " + departmentName);
    System.out.println("The number of visits is " + numberOfVisits);
    System.out.println("Class starts at " + timeStart);
    System.out.println("The name of your instructor is " + instructorName);
    System.out.println("The number of students in the class is " + numberOfStudents);
    
    
    //scan.close(); //closes the scanner
  } //main
			
	} //class

