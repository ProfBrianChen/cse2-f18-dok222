import java.util.Scanner;

public class HW10 {

    public static int inputs(int[] usedInputs,int check) { //checks user input
        Scanner scan = new Scanner(System.in); //scanner

        boolean finalizeInput = false; //boolean to check user input
        int userInput = -99; //user input

        do { //checks every condition
            if (scan.hasNextInt()) { //checks whether user has input an integer
                userInput = scan.nextInt(); //if input is integer then set that value to userInput
                finalizeInput = true; //sets boolean to true, enables leaving of while loop
            } else { //the input is not an integer
                System.out.println("The number is not an int");
                scan.next(); //scans next value
                finalizeInput = false; //input is not allowed to leave while loop
            }


            if (userInput > 9 || userInput < 1) { //checks if user input is not between 1 and 9 inclusively
                System.out.println("This number is not between 1 and 9, please try again");
                finalizeInput = false; //input is not allowed to leave loop
            }

            for(int i = 0; i < usedInputs.length;i++) { //for loop to check previous inputs array
                if(usedInputs[i] == userInput) { //if user input is repeated
                    finalizeInput = false; //input is no allowed to leave loop
                    System.out.println("This position has been used before");
                    break; //breaks out of for loop
                }
            }
        } while(finalizeInput == false); //runs while input is not within the requirements
        usedInputs[check] = userInput; //sets correct input to value in used inputs array
        return userInput; //returns user input
    }
    public static boolean check(int [][] board) { //checks if someone has won
        int checker = 0; //input to check tie
        for(int i = 0; i < board.length;i++) { //for loop that checks horizontal victories
            if(board[i][0] + board[i][1] + board[i][2] == 237) { //if the sum of a horizontal line is equivalent to 3 X's in ASCII
                System.out.println("X wins");
                return false; //returns false to end game
            } else if (board[i][0] + board[i][1] + board[i][2] == 264) { //if the sum of a horizontal line is equivalent to 3 O's in ASCII
                System.out.println("O wins");
                return false; //returns false to end game
            }
        }

        for(int i = 0; i < board.length;i++) { //for loop that checks vertical victories
            if(board[0][i] + board[1][i] + board[2][i] == 237) { //if the sum of a vertical line is equivalent to 3 X's in ASCII
                System.out.println("X wins");
                return false; //returns false to end game
            } else if (board[0][i] + board[1][i] + board[2][i] == 264) { //if the sum of a vertical line is equivalent to 3 O's in ASCII
                System.out.println("O wins");
                return false; //returns false to end game
            }
        }

        if(board[0][0] + board[1][1] + board[2][2] == 237) { //if the sum of a diagonal line is equivalent to 3 X's in ASCII
            System.out.println("X wins");
            return false; //returns false to end the game
        }else if(board[0][0] + board[1][1] + board[2][2] == 264) { //if the sum of a diagonal line is equivalent to 3 O's in ASCII
            System.out.println("O wins");
            return false; //returns false to end the game
        }

        if(board[0][2] + board[1][1] + board[2][0] == 237) { //if the sum of a diagonal line is equivalent to 3 X's in ASCII
            System.out.println("X wins");
            return false; //returns false to end the game
        }else if(board[0][2] + board[1][1] + board[2][0] == 264) { //if the sum of a diagonal line is equivalent to 3 O's in ASCII
            System.out.println("O wins");
            return false; //returns false to end the game
        }
        for(int i = 0; i < board.length;i++) { //for loop that increments checker if value in array is X or O
            for(int j = 0; j < board.length;j++) {
                if (board[i][j] == 79 || board[i][j] == 88) { //goes through every value to check for equivalence
                    checker++; //checker increments by 1
                }
            }
        }
        if(checker == 9) { //if every value in array is either X or O, game ends in tie
            System.out.println("This is a tie"); 
            return false; //returns false to end the game
        }
        return true; //returns true if winning or tieing conditions are not met
    }

    public static int[][] drawBoard(int [][] board) { //draws the game board
        for(int i = 0; i < board.length; i++) { //for loop that deals with rows
            for(int j = 0; j < board.length; j++) { //for loop that deals with columns
                if(board[i][j] == 88) { //if value on board is O, print O
                    System.out.print("O     ");
                } else if (board[i][j] == 79) { //if value on board is X, print X
                    System.out.print("X     ");
                }else { //if neither, keep original numbers
                    System.out.print(board[i][j] + "     ");
                }
            }
            System.out.println("");
        }

        return board; //returns drawn board
    }

    public static int[][] changeBoard(int [][] board, int character, int check, int[] usedinputs) { //changes board
        int baseBoard = 1; //starts at 1 to create the clean board
        if (check == 0) { //draws initial clean board if first time running method
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board.length; j++) {
                    board[i][j] = baseBoard; //sets values in array from 1 to 9
                    baseBoard++; //base board increments
                }
            }
        }
        baseBoard = 1; //resets baseboard for changes after initial clean board
        drawBoard(board); //draws board
        System.out.println("Please enter a spot to take:");
        int userInput = inputs(usedinputs,check); //takes in user input

        if(check == 0){ //if first run, change after input and clean board
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board.length; j++) {
                    if (baseBoard == userInput) { //if user input is equal to part of board from 1 to 9
                        board[i][j] = character; //set that value to either X or O according the last user
                    } else {
                        board[i][j] = baseBoard; //print the number assigned there
                    }
                    baseBoard++; //base board increments
                }
            }
        } else { //if not the first run, do not remake the numbers on clean board, only change where user inputs
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board.length; j++) {
                    if (baseBoard == userInput) {
                        board[i][j] = character;
                    }
                    baseBoard++;
                }
            }
        }
        return board; //returns changed board
    }


    public static void main(String[] args) {
        int turn = 0; //checks whose turn it is
        char character; //X or O
        int [][] ticTac1 = new int[3][3]; //game board array
        int [] usedInputs = new int[10]; //previously used inputs array

        while(check(ticTac1)) { //runs while there is no winner or tie
            if (turn % 2 == 0) { //alternates X and O
                character = 'O'; //sets character to O
            } else {
                character = 'X'; //sets character to X
            }
            ticTac1 = changeBoard(ticTac1, character, turn, usedInputs); //sets array to changed array through changeBoard
            turn++; //increments turn to change X to O and O to X
        }
        drawBoard(ticTac1); //Draws final Board

    } //main
} //class
