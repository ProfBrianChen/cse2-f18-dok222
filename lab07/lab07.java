import java.util.Random; //import random
import java.util.Scanner; //import scanner 

public class lab07 {

	public static String adjectives() { //Method generates adjectives
    Random randomGenerator = new Random(); //creates a random generator
    
		int randomInt = randomGenerator.nextInt(10); //generates random number between 0 and 9 inlcusive
		String adjective = ""; //create adjective variable

		switch(randomInt) { //begin switch statement that inputs random number
			case 0:
				adjective = "sleepy";
			    break;
			case 1:
				adjective = "slimey";
				break;
			case 2:
				adjective = "stinky";
				break;
			case 3:
				adjective = "sloppy";
				break;
			case 4:
				adjective = "silly";
				break;
			case 5:
				adjective = "stumpy";
				break;
			case 6:
				adjective = "sexy";
				break;
			case 7:
				adjective = "speedy";
				break;
			case 8:
				adjective = "silky";
				break;
			case 9:
				adjective = "salty";
				break;
		}
		return adjective; //returns adjective
	}

	public static String subjectNouns() { //Method generates nouns
		Random randomGenerator = new Random();
    
		int randomInt = randomGenerator.nextInt(10);
		String subjectNoun = ""; //create subject noun variable
		
		switch(randomInt) { //begin switch statement that inputs random number
			case 0:
				subjectNoun = "dog";
				break;
			case 1:
				subjectNoun = "cat";
				break;
			case 2:
				subjectNoun = "rabbit";
				break;
			case 3:
				subjectNoun = "elephant";
				break;
			case 4:
				subjectNoun = "giraffe";
				break;
			case 5:
				subjectNoun = "rat";
				break;
			case 6:
				subjectNoun = "hippo";
				break;
			case 7:
				subjectNoun = "pigeon";
				break;
			case 8:
				subjectNoun = "lizard";
				break;
			case 9:
				subjectNoun = "dinosaur";
				break;
		}
		
		return subjectNoun; //returns noun
	}

	public static String verbs() { //Method generates verbs
		Random randomGenerator = new Random();

		int randomInt = randomGenerator.nextInt(10);
		String verb = ""; //create verb variable
		
		switch(randomInt) { //begin switch statement that inputs random number
			case 0:
				verb = "ate";
				break;
			case 1:
				verb = "swam";
				break;
			case 2:
				verb = "spammed";
				break;
			case 3:
				verb = "scammed";
				break;
			case 4:
				verb = "sprung";
				break;
			case 5:
				verb = "stung";
				break;
			case 6:
				verb = "slipped";
				break;
			case 7:
				verb = "skipped";
				break;
			case 8:
				verb = "sat";
				break;
			case 9:
				verb = "scorched";
				break;
		}
		return verb; //returns verb
	}

	public static String objectNouns() { //Method generates nouns
		Random randomGenerator = new Random();
    
		int randomInt = randomGenerator.nextInt(10);
		String objectNoun = ""; //create noun variable
		
		switch(randomInt) { //begin switch statement that inputs random number
			case 0:
				objectNoun = "lion";
				break;
			case 1:
				objectNoun = "swan";
				break;
			case 2:
				objectNoun = "rhino";
				break;
			case 3:
				objectNoun = "raccoon";
				break;
			case 4:
				objectNoun = "bear";
				break;
			case 5:
				objectNoun = "skunk";
				break;
			case 6:
				objectNoun = "fish";
				break;
			case 7:
	  		objectNoun = "squirrel";
				break;
			case 8:
				objectNoun = "crane";
				break;
			case 9:
				objectNoun = "chicken";
				break;
	}
		
		return objectNoun; //returns noun
	}

	public static String createSentence() { //creates intro sentence
		String subjectWord = objectNouns(); //sets a noun to a variable
		System.out.println("The " + adjectives() + " " + adjectives() + " " + subjectWord + " " + verbs() + " the " + adjectives() + " " + subjectNouns()); //calls multiple methods to create sentence
		
		return subjectWord; //returns previously saved word
		}
  
  public static String supportingSentence(String subjectWord) { //creates an action sentence that takes a string
    Random randomGenerator = new Random();
    
    int randomInt = randomGenerator.nextInt(2);//generates random number between 0 and 1
    
    if (randomInt == 0) { //if random number is 0 then the noun is used
      System.out.println("This " + subjectWord + " was particularly " + adjectives() + " to " + verbs() + " " + subjectNouns() + ".");
    } else { //if random number is 1 then 'it' is used in place of the noun
      System.out.println("It used " + objectNouns() + " to " + verbs() + " " + objectNouns() + " at the " + adjectives() + subjectNouns());
    }
    return subjectWord; //returns noun from createSentence method
  }
  
  public static void concludingSentence(String subjectWord) { //creates a method that ends the paragraph, takes an inputs
    System.out.println("That " + subjectWord + " " + verbs() + " " + objectNouns() + "!"); //creates a sentence that takes input
    
    return; 
  }
  
  public static void constructParagraph() { //constructs a paragraph
    Random randomGenerator = new Random();
    
    int randomInt = randomGenerator.nextInt(8); //generates a random number between 0 and 7
      
    String subjectWord = createSentence(); //takes return value from create sentence and sets to variable
    
    for(int i = 0; i < randomInt; i++) { //loops random int number of times
      supportingSentence(subjectWord); //calls supportingSentence method with original return from create sentence
    }
    
    concludingSentence(subjectWord); //calls concludingSentence method with original return from create sentence
    
  }
  
	public static void main(String[] args) { //main
		Scanner scan = new Scanner(System.in); //creates a new scanner
		
		int numSent = 1; //int to check if user wants more sentences
    
		do { //loop that runs at least once
			System.out.println("The " + adjectives() + " " + adjectives() + " " + objectNouns() + " " + verbs() + " the " + adjectives() + " " + subjectNouns()); //creates a sentence while calling to multiple methods
			System.out.println("Would you like another sentence? Enter 1 for yes or 2 for no."); //asks user for input
			numSent = scan.nextInt(); //sets user input to check
		}while(numSent == 1); //runs while the user sets their input to 1
		
    constructParagraph(); //runs constructParagraph method
	} //main
  
} //class