//Daniel Karkhut CSE 002

/*This program converts two inputs to calculate the cubic miles
*that a hurrican effects. Two inputs given by the user, number
*of acres of land affected by the hurricane and the average
*inches of rain dropped by the hurricane. The quantities
*are found using the scanner method. The quantities are 
* taken and converted into cubic miles. 
*/
/////////
 import java.util.Scanner;
//
public class Convert{
   
  public static void main(String[] args) {
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print("Enter the affected area in acres: "); //prompt user for affected area
    double affectedArea = myScanner.nextDouble(); //next input is set to affected area in acres
    
    System.out.print("Enter the rainfall in the affected area: "); //prompt user for inches of rainfall
    double amountOfRainfall  = myScanner.nextDouble(); //next double is set to the amount of rainfall in inches
    
    double gallonsPerAcreInch = 27154.285990761; //number of gallons in an acre*inch
    double squareMilesPerGallon = 0.000000000000908169; //number of square miles in a gallon
    
    double volumeOfRain = ((affectedArea * amountOfRainfall) * gallonsPerAcreInch) * squareMilesPerGallon; //calculate and convert volume of rain from acre*inch to gallon to square miles 
    System.out.println(volumeOfRain + " cubic miles"); //print the volume of rain to screen
    
  }
  
}