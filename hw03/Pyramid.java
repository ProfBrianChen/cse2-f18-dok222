//Daniel Karkhut CSE 002

/*This program converts two inputs to calculate the volume of a
*triangle. Two inputs given by the user, the length of a side of 
*the square face of the pyramid and the height of the pyramid. 
*The quantities are found using the scanner method. The quantities are 
*taken and placed into the formula for the volume of a pyramid.  
*/
/////////
 import java.util.Scanner;
//
public class Pyramid{
   
  public static void main(String[] args) {
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print("The square side of the pyramid is (input length) : "); //prompt user for length of side on square side of pyramid
    double squareSideOfPyramid = myScanner.nextDouble(); //next input is set to square side of pyramid
    
    System.out.print("The height of the pyramid is (input height) : "); //prompt user for height of the pyramid
    double heightOfPyramid = myScanner.nextDouble(); //next double is set to the height of a pyramid
   
    double volumeOfPyramid = (Math.pow(squareSideOfPyramid, 2) * heightOfPyramid) / 3; //calculate the volume of pyramid using the volume of pyramid formula 
    System.out.println("The volume inside the pyramid is: " + volumeOfPyramid + "."); //print the volume of pyramid to screen
    
  }
  
}