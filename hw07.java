
import java.util.Scanner; //imports scanner

public class hw07{

  public static String sampleText() {
    Scanner scan = new Scanner(System.in);
    String a = "";
    System.out.println("Enter a sample text:");
    a = scan.nextLine();
    return a;
  }
  /////////////
  public static String printMenu(String input1) { //check this piece of code
    Scanner scan = new Scanner(System.in);
    
    String userChoiceFromMenu = "";
    int numOfWSCharacters;
    int numOfWords;
    int instancesOfText;
    boolean runMenu = true;
    
    do{
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Choose an option:");
    
    userChoiceFromMenu = scan.nextLine();  
    switch (userChoiceFromMenu) {
      case "c":
        numOfWSCharacters = getNumOfNonWSCharacters(input1);
        System.out.println("Number of non-whitespace");
        System.out.println("characters: " + numOfWSCharacters);
        break;
      case "w":
        numOfWords = getNumOfWords(input1);
        System.out.println("Number of words: " + numOfWords);
        break;
      case "f":
        System.out.println("Enter a word or phrase to be found");
        String userText = scan.nextLine();
        instancesOfText = findText(input1, userText);
        System.out.println("\"" + userText + "\"" + " instances: " + instancesOfText);
        break;
      case "r":
        replaceExclamation(input1);
        break;
      case "s":
        shortenSpace(input1);
        break;
      case "q":
        runMenu = false;
        break;
      default:
        System.out.println("This is an incorrect input. Try again.");
        continue;
    }
    }while (runMenu == true);
    return userChoiceFromMenu;
    }
  /////////////
  public static int getNumOfNonWSCharacters(String input2) {
    
    int spaces = input2.replaceAll("[^ ]", "").length();
    return spaces;
  }
  /////////////
  public static int getNumOfWords(String input3) {
    
    int words = 0;
    for (int i = 0; i < input3.length()-1;i++) {
      if (input3.charAt(i) == ' ' && input3.charAt(i + 1) != ' ') {
        words++;  
      }  
    }
    words++;
    return words;
  }
  /////////////
  public static int findText(String input4, String textToBeFound){
    
    int instancesOfText = 0;
    
    while(input4.contains(textToBeFound)) {
      input4 = input4.replaceFirst(textToBeFound, " ");
      instancesOfText++;
    }
    return instancesOfText;
  }
  /////////////
  public static String replaceExclamation(String input5) {
    
    String inputWithoutExclamation = "";
    
    inputWithoutExclamation = input5.replaceAll("!", ".");
    System.out.println("Edited text: " + inputWithoutExclamation);
    return inputWithoutExclamation;
  }
  /////////////
  public static String shortenSpace(String input6) {
    String inputWithShortSpace = "";
    
    inputWithShortSpace = input6.replaceAll("\\s+", " ");
    System.out.println("Edited text: " + inputWithShortSpace);
    return inputWithShortSpace;
  }
  /////////////
  public static void main (String[]args){
  
  Scanner scan = new Scanner(System.in); //initialize scanner
  String userInput = "";
  userInput = sampleText();
  System.out.println("You entered: " + userInput);
  printMenu(userInput);
    
    
 }// end of main method 
} // end of class