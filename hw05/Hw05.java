//Daniel Karkhut CSE 002 hw05
/* This program generates the probability
of certain hands of poker to show up in any
number of hands. The user is asked how many hands
they want to play. Then, 5 cards are created and given
face values which establish the card's value. These are
then checked to decide if there extsts a four of a kind,
a three of a kind, two pairs, or a single pair. Then, the 
probability of each combination is decided and output.
*/
import java.lang.Math;
import java.util.Scanner;

public class Hw05 {
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    
    int fourOfAKind = 0; //initialzes the number of four of a kinds
    int threeOfAKind = 0; //initializes the nubmer of three of a kinds 
    int singlePair = 0; //initializes the number of individual pairs
    int twoPair = 0; //initializes the number of two pairs
    int numberOfPlayers; //the number of players, value changes
    System.out.println("How many players are playing?");
    numberOfPlayers = scan.nextInt();
    int numberOfPlayersProb = numberOfPlayers; //the number of players, value is static
    
    while(numberOfPlayers > 0) { //runs for the amount of hands being dealt
    
      int card1 = (int) (Math.random() * ((52 - 1) + 1)) + 1; //assigns random number between 1 and 52
      int card2 = (int) (Math.random() * ((52 - 1) + 1)) + 1;
      
      while (card1 == card2) { //checks whether the card is a duplicate
        card2 = (int) (Math.random() * ((52 - 1) + 1)) + 1;
      }
      
      int card3 = (int) (Math.random() * ((52 - 1) + 1)) + 1;
      while (card3 == card2 || card3 == card1) { //checks whether the card is a duplicate
        card3 = (int) (Math.random() * ((52 - 1) + 1)) + 1;
      }
     
      int card4 = (int) (Math.random() * ((52 - 1) + 1)) + 1;
      while (card4 == card3 || card4 == card2 || card4 == card1) { //checks whether the card is a duplicate
        card4 = (int) (Math.random() * ((52 - 1) + 1)) + 1;
      }
      
      int card5 = (int) (Math.random() * ((52 - 1) + 1)) + 1;
      while (card5 == card4 || card5 == card3 || card5 == card2 || card5 == card1) { //checks whether the card is a duplicate
        card5 =(int) (Math.random() * ((52 - 1) + 1)) + 1;
      }
      
      // checks whether there is a four of a kind
     if ((card1 % 13 == card2 % 13 && card2 % 13 == card3 % 13 && card3 % 13 == card4 % 13) || 
         (card2 % 13 == card3 % 13 && card3 % 13 == card4 % 13 && card4 % 13 == card5 % 13) ||
         (card3 % 13 == card4 % 13 && card4 % 13 == card5 % 13 && card5 % 13 == card1 % 13) ||
         (card4 % 13 == card5 % 13 && card5 % 13 == card1 % 13 && card1 % 13 == card2 % 13) ||
         (card5 % 13 == card1 % 13 && card1 % 13 == card2 % 13 && card2 % 13 == card3 % 13)){
       
       fourOfAKind++; //increments when four of a kind is found
     }
      
      // checks whether there is a three of a kind
      if ((card1 % 13 == card2 % 13 && card2 % 13 == card3 % 13) ||
          (card2 % 13 == card3 % 13 && card3 % 13 == card4 % 13) ||
          (card3 % 13 == card4 % 13 && card4 % 13 == card5 % 13) ||
          (card4 % 13 == card5 % 13 && card5 % 13 == card1 % 13) ||
          (card5 % 13 == card1 % 13 && card1 % 13 == card2 % 13) ||
          (card1 % 13 == card3 % 13 && card3 % 13 == card3 % 13) ||
          (card1 % 13 == card2 % 13 && card2 % 13 == card4 % 13) ||
          (card2 % 13 == card3 % 13 && card3 % 13 == card5 % 13) ||
          (card2 % 13 == card4 % 13 && card4 % 13 == card5 % 13) ||
          (card1 % 13 == card3 % 13 && card3 % 13 == card5 % 13)){

        threeOfAKind++; //increments when three of a kind is found
      }
          
     // checks whether there are two pairs
      if ((card1 % 13 == card2 % 13 && card3 % 13 == card4 % 13)||
          (card1 % 13 == card2 % 13 && card3 % 13 == card5 % 13)||
          (card1 % 13 == card2 % 13 && card4 % 13 == card5 % 13)||
          (card1 % 13 == card3 % 13 && card2 % 13 == card4 % 13)||
          (card1 % 13 == card3 % 13 && card2 % 13 == card5 % 13)||
          (card1 % 13 == card3 % 13 && card4 % 13 == card5 % 13)||
          (card1 % 13 == card4 % 13 && card2 % 13 == card3 % 13)||
          (card1 % 13 == card4 % 13 && card2 % 13 == card5 % 13)||
          (card1 % 13 == card4 % 13 && card3 % 13 == card5 % 13)||
          (card1 % 13 == card5 % 13 && card2 % 13 == card3 % 13)||
          (card1 % 13 == card5 % 13 && card2 % 13 == card4 % 13)||
          (card1 % 13 == card5 % 13 && card3 % 13 == card4 % 13)||
          (card2 % 13 == card3 % 13 && card4 % 13 == card5 % 13)||
          (card3 % 13 == card4 % 13 && card2 % 13 == card5 % 13)) {

         twoPair++; //increments when three of a kind is found
      }
    	  
      // checks whether there is a pair
      if ((card1 % 13 == card2 % 13 && card3 % 13 != card4 % 13 && card4 % 13 != card5 % 13 && card3 % 13 != card5 % 13)||
          (card1 % 13 == card3 % 13 && card2 % 13 != card4 % 13 && card4 % 13 != card5 % 13 && card2 % 13 != card5 % 13)||
          (card1 % 13 == card4 % 13 && card2 % 13 != card3 % 13 && card3 % 13 != card5 % 13 && card2 % 13 != card5 % 13)||
          (card1 % 13 == card5 % 13 && card2 % 13 != card3 % 13 && card3 % 13 != card4 % 13 && card2 % 13 != card4 % 13)||
          (card2 % 13 == card3 % 13 && card1 % 13 != card4 % 13 && card4 % 13 != card5 % 13 && card1 % 13 != card5 % 13)||
          (card2 % 13 == card4 % 13 && card1 % 13 != card3 % 13 && card3 % 13 != card5 % 13 && card1 % 13 != card5 % 13)||
          (card2 % 13 == card5 % 13 && card1 % 13 != card3 % 13 && card3 % 13 != card4 % 13 && card1 % 13 != card4 % 13)||
          (card3 % 13 == card4 % 13 && card1 % 13 != card2 % 13 && card2 % 13 != card5 % 13 && card1 % 13 != card5 % 13)||
          (card3 % 13 == card5 % 13 && card1 % 13 != card2 % 13 && card2 % 13 != card4 % 13 && card1 % 13 != card4 % 13)||
          (card4 % 13 == card5 % 13 && card1 % 13 != card2 % 13 && card2 % 13 != card3 % 13 && card1 % 13 != card3 % 13)){

    	  singlePair++; //increments when three of a kind is found
      }
  
      numberOfPlayers--; //decrements to make sure only the number of hands is checked
    }
    
    double fourOfAKindProb = (double) fourOfAKind/numberOfPlayersProb; //calculates the probability of four of a kinds
    double threeOfAKindProb = (double) threeOfAKind/numberOfPlayersProb; //calculates the probability of three of a kinds
    double twoPairProb = (double) twoPair/numberOfPlayersProb; //calculates the probability of two pairs
    double singlePairProb = (double) singlePair/numberOfPlayersProb; //calculates the probability of single pairs

    System.out.println("The number of players in this game is " + numberOfPlayersProb);
    System.out.println("The probability of a hand with four of a kind is " + fourOfAKindProb);
    System.out.println("The probability of a hand with three of a kind is " + threeOfAKindProb);
    System.out.println("The probability of a hand with two pairs is " + twoPairProb);
    System.out.println("The probability of a hand with a pair is " + singlePairProb);
  
 
     
  }//main
}//class
