//Daniel Karkhut
//September 7th, 2018
//CSE 02
/* This program is meant simulate a bicycle's cyclometer 
which measures the speed and distance of the bike. This 
program records both time elapsed in second and the 
number of rotations of the front wheel. It will print
the number of minutes elapsed in a trip, the number of 
rotations per trip, the distance traveled per trip,
and the sum of two trips. */
//
public class Cyclometer {
  
  public static void main(String[] args) {
    int secsTrip1 = 480; //seconds elapsed for trip 1
    int secsTrip2 = 3220; //seconds elapsed for trip 2
    int countsTrip1 = 1561; //number of rotations for trip 1
    int countsTrip2 = 9037; //number of rotations for trip 2
    
    double wheelDiameter = 27.0, //diameter of the wheel
    PI = 3.14159, //Value for PI constant
    feetPerMile = 5280, //number of feet in a mile
    inchesPerFoot = 12, //number of inches in a foot
    secondsPerMinute = 60; //number of seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance; //Total distances of trips 1, 2, and 1 + 2
    
    //Print out time per trip in minutes and counts per trip
    System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
    //Used to run and store values for the distances of trips 1, 2, and the total
    //This piece calculates the distance by multiplying the cirumference by the number of rotations. Given in inches 
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    distanceTrip1 /= inchesPerFoot * feetPerMile; //converts inches to feet and then feet into miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //same process as distance for trip 1 expect using the 2 trip's values. Given in miles
    totalDistance = distanceTrip1 + distanceTrip2; //adds distance traveled of trip 1 to the distance traveled of trip 2 for total distance
    
    //Print out the output data.
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");
    
    
  }
}
