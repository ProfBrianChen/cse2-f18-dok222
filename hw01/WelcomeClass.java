//////////////
//// CSE 02 WelcomeClass Daniel Karkhut
///
public class WelcomeClass{
  //prints Lehigh username to screen
  public static void main(String args[]){
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-D--O--K--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v"); 
    
  }
}