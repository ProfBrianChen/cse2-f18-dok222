/* Daniel Karkhut CSE 002 HW 09
   This is a program that uses binary search and linear
   search to look through various arrays to check for certain
   values. First, inputs are taken from the user and input into
   an array after each value is checked to be between 0 and 100,
   an integer, and ascending in value. Then this array runs through
   a binary search after which the array is scrambled into a new
   array that has randomly placed values from the first array. Then
   this array is searched through a linear search tool.
   *Inputs are check if user hits enter after every input and loops forever*
*/

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {

    public static int binarySearch(int[] list, int key) { //binary search method that prints how long it takes to find certain value
        int low = 0; //low value
        int high = list.length - 1; //high value
        int iterations = 0; //number of iterations used
        while (high >= low) { //runs as long has high is bigger than low
            int mid = (low + high) / 2; //mid is set to the middle value of array
            if (key < list[mid]) { //if user input is less than mid index
                high = mid - 1; //high is set to mid minus 1
            } else if (key == list[mid]) { //if user value is found directly at mid
                System.out.print(key + " was found in the list with " + iterations + " iterations"); //print how many iterations it took to find
                return mid;
            } else {
                low = mid + 1; //low is set to mid plus 1
            }
            iterations ++; //increments number of iterations after each run
        }
        System.out.println(key + " was not found in the list with " + iterations + " iterations"); //prints if no iterations were found
        return -1;
    }

    public static int[] ScrambledArray(int[] array1) { //shuffle array, shuffles an array
        Random rand = new Random(); //allows for random number generation
        int index = 0; //index
        int x = 0; //creates a variable to hold values
        for(int i = 0; i < 15;i++) { //for loop that runs 15 times
            index = rand.nextInt(15); //generates a random number between 14 and 0
            x = array1[index]; //sets random array index to x
            array1[index] = array1[i]; //random array index is set to the i index of array
            array1[i] = x; //i index of array is set to x
        }
        System.out.println("Scrambled");
        for (int j = 0; j < array1.length; j++) { //prints array
            System.out.print(array1[j]);
            System.out.print(" ");
        }
        return array1; //returns array1
    }

        public static int linearSearch(int[] list, int key) { //linear search that goes through each value one by one
            int iterations = 0; //number of iterations
            for (int i = 0; i < list.length; i++) { //checks each index starting at 0
                if (key == list[i]) { //when key is equal to that index
                    System.out.print(key + " was found after " + iterations + " iterations"); //prints the amount of iterations to find value
                    return i;
                }
                iterations++; //increments number of iterations
            }
            System.out.print(key + " was not found after " + iterations + " iterations"); //occurs if value is not found
            return -1;
        }

    public static void main(String[] args) { //main method
        Scanner scan = new Scanner(System.in); //creates a scanner

        int gradeArray[]; //creates an array
        int grade1 = 0; //grade1 is set to 0 initially, placeholder for checks
        gradeArray = new int[15]; //sets array length to 15

        System.out.println("Enter 15 digits");
            for (int i = 0; i < 15; i++) { //for loop that checks user input
                if (scan.hasNextInt()) { //checks whether user has input an integer
                    grade1 = scan.nextInt(); //if input is integer then set that value to grade1
                } else { //the input is not an integer
                    System.out.println("The number is not an int");
                    scan.next(); //scans next value
                    i = i -1; //decrements i to reset user input one step back
                    continue; //loops the for loop
                }

                if (grade1 > 100 || grade1 < 0) { //checks whether input is between 100 and 0
                    System.out.println("The number is not in the range");
                    i = i - 1; //decrement i to reset user input one step back
                    continue; //loops the for loop
                }

                if (i > 0) { //checks so that the first index is not used
                    if (grade1 <= gradeArray[i - 1]) { //checks whether the input is larger than the one used previously
                        System.out.println("This number is not ascending");
                        i = i - 1; //decrements i by 1 to reset user input one setp
                        continue; //resets loop
                    }
                }
                gradeArray[i] = grade1; //finally sets index of array to input if each check isn't triggered
            }
        for (int j = 0; j < gradeArray.length; j++) { //prints out array
            System.out.print(gradeArray[j]);
            System.out.print(" ");
        }

        System.out.println("");

        System.out.print("Enter a grade to search for: ");
        int key = scan.nextInt(); //prompts user for key
        binarySearch(gradeArray, key); //runs binary search with grade array and user input key
        System.out.println("");
        int scrambled[] = ScrambledArray(gradeArray); //runs scrambled array and returns new array and sets it to new array
        System.out.println("");
        System.out.println("Enter a grade to search for: ");
        key = scan.nextInt(); //asks user for new key
        linearSearch(scrambled, key); //runs linear search that searches the new scrambled array

    }//main
}//class

