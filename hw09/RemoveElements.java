/* Daniel Karkhut CSE 002 HW 09
   This program is meant to manipulate arrays by removing
   certain values and creating new arrays with user inputs.
   First, an array is created out of random numbers in a method.
   Then, this array is put into a method that asks a user for
   and index and then removes that index, decreasing the size of the
   array by 1 and keeping all the old values while losing the user's index
   value. Then, the original array is taken and the user inputs a value that
   is removed in every instance from the old array, creating a new array
   that has decreased in size but kept the old values that were not chosen.
   This loops forever as long as the user inputs y at the end to loop again.
 */

import java.util.Scanner;
import java.util.Random;

public class RemoveElements{

    public static int[] randomInput() { //creates and sets array to random values
        Random rand = new Random(); //enables random number generation
        int array1[]; //creates array
        array1 = new int[10]; //creates an array with size 10

        for(int i = 0; i < array1.length; i++) { //for loop
            array1[i] = rand.nextInt(10); //gives each index a random number between 0 and 10
        }
        return array1; //returns array
    }

    public static int[] delete(int num[], int index) { //deletes user picked value and creates a new array without it
        int array1[];
        array1 = new int[9]; //creates array with 9 values
        if(index > 10 || index < 0) { //checks if user input has valid index
            System.out.println("The index is not valid.");
        } else {
            System.out.println("Index " + index + " element is removed");
        }
        for(int i = 0; i < array1.length;i++) { //skips chosen value and creates new array with that index missing
            if(i < index) { //checks if index is less
                array1[i] = num[i]; //sets array1 index to num index
            } else if(i >= index){ //once chosen index is reached, that value is skipped and the next values are used
                array1[i] = num[i+1];
            }
        }
        return array1; //returns array
    }

    public static int[] remove(int num[], int target) { //takes user picked number and removes from array and then used to create new array
        int array1[];
        array1 = new int[10]; //creates array with size 10
        int index1 = 0; //index1 value
        int count = 0; //counts to check for printed statements
        for(int i = 0; i < array1.length;i++) { //if target value is in array
            if (num[i] != target) { //if value is not target, array 1 index is equal to num index
                array1[index1] = num[i];
                index1++; //increments to change index
            } else {
                count++; //increments for print statements
            }
        }
        if(count > 0) { //prints if the target is found or not
            System.out.println("Element " + target + " has been found");
        } else {
            System.out.println("Element " + target + " has not been found");
        }

        int array2[]; //new array
        array2 = new int[index1]; //sets array2 size to the size of array1 without target value
        for(int i = 0; i < array2.length;i++) { //sets every value of array1 to array 2
            array2[i] = array1[i];
        }
        return array2; //returns array2
    }

    public static void main(String [] arg){
        Scanner scan=new Scanner(System.in);
        int num[]=new int[10];
        int newArray1[];
        int newArray2[];
        int index,target;
        String answer="";
        do{
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput();
            System.out.println("");
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num,index);
            String out1="The output array is ";
            out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"
            System.out.println(out1);
            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num,target);
            String out2="The output array is ";
            out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer=scan.next();
        }while(answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]){
        String out="{";
        for(int j=0;j<num.length;j++){
            if(j>0){
                out+=", ";
            }
            out+=num[j];
        }
        out+="} ";
        return out;
    }
}
