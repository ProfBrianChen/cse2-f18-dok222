import java.util.Scanner;
import java.util.Random;
public class hw08{

    public static String[] printArray(String[] array1) { //prints array
        for(int i = 0; i < array1.length; i++) { //loops the length of the array
            System.out.print(array1[i] + " "); //prints array indexes from 0 to last index in the array
        }
        System.out.println(""); //next line
        return array1; //returns array
    }

    public static String[] shuffle(String[] array1) { //shuffle array, shuffles an array
        Random rand = new Random(); //allows for random number generation
        int index = 0; //index
        String x = ""; //creates a variable to hold values
        for(int i = 0; i < 52;i++) { //for loop that runs 52 times
            index = rand.nextInt(51 - 0 + 1); //generates a random number between 51 and 0
            x = array1[index]; //sets random array index to x
            array1[index] = array1[i]; //random array index is set to the i index of array
            array1[i] = x; //i index of array is set to x
        }
        System.out.println("Shuffled");
        return array1; //returns array1
    }

    public static String[] getHand(String[] array1, int index, int numCards) { //array that takes numCards value out of input array and into a new array
        String[] array2 = new String[numCards]; //array with length numCards
        System.out.println("hand");
        for(int i = 0; i < numCards; i++) { //loop that sets last index of array1 to first index of array1 and increments
           array2[i] = array1[index - i]; //index of array2 decrements by 1 as array1 increments by 1
        }

        return array2; //returns array2
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //suits club, heart, spade or diamond
        String[] suitNames={"C","H","S","D"}; //string array that creates suites
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //string array that creates ranks
        String[] cards = new String[52]; //new empty string array with size of 52
        String[] hand = new String[5]; //new empty string array with size of 5
        int numCards = 5; //number of cards drawn in the hand
        int again = 1; //checks whether code should run again
        int index = 51; //index for assigning arrays
        for (int i=0; i<52; i++){ //for loop that creates every suit and rank combination in cards array
            cards[i]=rankNames[i%13]+suitNames[i/13];
            //System.out.print(cards[i]+" ");
        }
        System.out.println(); //next line
        printArray(cards); //returns array
        shuffle(cards); //returns array
        printArray(cards); //returns array
        while(again == 1){ //loops pulling of hand
            if (index < 5) {
                for (int i = 0; i < 52; i++) { //for loop that creates every suit and rank combination in cards array
                    cards[i] = rankNames[i % 13] + suitNames[i / 13];
                }
                shuffle(cards);
                index = 51;
            }
            hand = getHand(cards,index,numCards); //returns array
            printArray(hand); //prints array returned from getHand
            index = index - numCards; //subtracts from index to pull next 5 cards
            System.out.println("Enter a 1 if you want another hand drawn");
            again = scan.nextInt(); //checks if user wants another hand
        }
    } //main
} //class
