//Daniel Karkhut CSE 002 Lab 4
/* This program randomly picks a card from an average 52 
/card deck. The suit and number of the card must be
/established before hand with cards that ascend in order:
/Cards 1-13 represent diamonds, cards 14-26 are clubs, then
/hearts, and then spades.
*/
import java.lang.Math;

public class CardGenerator {
  
  public static void main(String[] args) {
    int randomNumber = (int) (Math.random() * ((52 - 1) + 1)) + 1; //declares this variable as a random number inclusivly between 1 and 52
    String suitOfCard; //suit of the card
    String identityOfCard; //number/rank on the card
    
    if (randomNumber <= 13) { //checks if random number is less than 13
      suitOfCard = "Diamonds"; //sets suit to diamonds
      switch (randomNumber) { //checks what random number value is to set to rank/number string, aka the identity of the card
        case 1:
          identityOfCard = "Ace";
          break;
        case 2:
          identityOfCard = "2";
          break;
        case 3:
          identityOfCard = "3";
          break;
        case 4:
          identityOfCard = "4";
          break;
        case 5:
          identityOfCard = "5";
          break;
        case 6:
          identityOfCard = "6";
          break;
        case 7:
          identityOfCard = "7";
          break;
        case 8:
          identityOfCard = "8";
          break;
        case 9:
          identityOfCard = "9";
          break;
        case 10:
          identityOfCard = "10";
          break;
        case 11:
          identityOfCard = "Jack";
          break;
        case 12:
          identityOfCard = "Queen";
          break;
        default:
          identityOfCard = "King";
          
      }
    }
    else if (randomNumber > 13 && randomNumber <= 26) { //checks if random number is between 13 and 26
      suitOfCard = "Clubs"; //sets suit of card to clubs 
      switch (randomNumber) { //checks random number value to set a string of rank/number to the identity
        case 14:
          identityOfCard = "Ace";
          break;
        case 15:
          identityOfCard = "2";
          break;
        case 16:
          identityOfCard = "3";
          break;
        case 17:
          identityOfCard = "4";
          break;
        case 18:
          identityOfCard = "5";
          break;
        case 19:
          identityOfCard = "6";
          break;
        case 20:
          identityOfCard = "7";
          break;
        case 21:
          identityOfCard = "8";
          break;
        case 22:
          identityOfCard = "9";
          break;
        case 23:
          identityOfCard = "10";
          break;
        case 24:
          identityOfCard = "Jack";
          break;
        case 25:
          identityOfCard = "Queen";
          break;
        default:
          identityOfCard = "King";
          
      }
    }
    else if (randomNumber > 26 && randomNumber <= 39) { //checks if random number is between 26 and 39
      suitOfCard = "Hearts"; //sets the suit of card to hearts
      switch (randomNumber) { //sets the identity of the card to a string of the rank/number
        case 27:
          identityOfCard = "Ace";
          break;
        case 28:
          identityOfCard = "2";
          break;
        case 29:
          identityOfCard = "3";
          break;
        case 30:
          identityOfCard = "4";
          break;
        case 31:
          identityOfCard = "5";
          break;
        case 32:
          identityOfCard = "6";
          break;
        case 33:
          identityOfCard = "7";
          break;
        case 34:
          identityOfCard = "8";
          break;
        case 35:
          identityOfCard = "9";
          break;
        case 36:
          identityOfCard = "10";
          break;
        case 37:
          identityOfCard = "Jack";
          break;
        case 38:
          identityOfCard = "Queen";
          break;
        default:
          identityOfCard = "King";
         
      }
    }
    else { //if the random number is over 39, this runs
      suitOfCard = "Spades"; //sets suit of the card to spades
      switch (randomNumber) { //sets the identity of the card to a string of the rank/number
        case 40:
          identityOfCard = "Ace";
          break;
        case 41:
          identityOfCard = "2";
          break;
        case 42:
          identityOfCard = "3";
          break;
        case 43:
          identityOfCard = "4";
          break;
        case 44:
          identityOfCard = "5";
          break;
        case 45:
          identityOfCard = "6";
          break;
        case 46:
          identityOfCard = "7";
          break;
        case 47:
          identityOfCard = "8";
          break;
        case 48:
          identityOfCard = "9";
          break;
        case 49:
          identityOfCard = "10";
          break;
        case 50:
          identityOfCard = "Jack";
          break;
        case 51:
          identityOfCard = "Queen";
          break;
        default:
          identityOfCard = "King";
          
      }
    }
  System.out.println("You picked the " + identityOfCard + " of " + suitOfCard); //prints out the identity and suit of the card
  }
}
