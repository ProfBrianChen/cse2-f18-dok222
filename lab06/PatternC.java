/*
 * Daniel Karkhut CSE002 Lab05
 * This code takes inputs from the user using
 * a scanner for various pieces of data on a
 * university course. Using while loops and if-else
 * statements, the inputs are checked to see if they 
 * are input with the correct type of data eg. string,
 * integer, and double. Then, while loops are used
 * to decide how many rows and columns are printed
 * on the screen. Lastly, the code inputs the data into
 * variables and then prints it to the screen.
 */

package lab06;
import java.util.Scanner; //importing scanner utility

public class PatternC {
	
	
	public static void main(String[] args) {
	
	Scanner scan = new Scanner(System.in); //creating a scanner
    
	int i = 0;
	System.out.println("Enter an integar between 1 and 10"); //asks user for input
	
	while (scan.hasNext()) { //runs while their is an input given by user
	      if (scan.hasNextInt()) { //checks whether the input is an integer
	    	 i = scan.nextInt(); //sets input value to variable
	    	 	if (i <= 10 && i >= 0) { //checks whether user input number between 1 and 10
	    	 		break;
	    	 	}else {
	    	 	  System.out.println("This is not the correct input, please try again."); //prompts user for another input
	    	 	}
	      } else { //runs when input is not an integer
	    	  System.out.println("This is not the correct input, please try again."); //prompts user for another input
	    	  scan.next(); //erases current input and goes back to the beginning of while loop
	      }
	    
	}
	
	int h;
	
	for(int k = 1;k <= i;k++) { //decides how many rows are created
		for(h = k;h < i; h++) {
			System.out.print(" "); //creates space in beginning of line. Decrements by one each time for loop runs
		}
		for(int j = k;j > 0;j--) { //decides how many columns are created
			System.out.print(j); //starts at k and decrements.
		}
		System.out.println(); //pushes text to next line
	}
	
    scan.close(); //closes the scanner
  } //main
			
	} //class

