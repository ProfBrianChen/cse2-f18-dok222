//
public class Arithmetic {
  
  public static void main(String[] args) {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltPrice = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    double totalCostOfPants; //total cost of pants
    totalCostOfPants = Math.round(((pantPrice * numPants) * (1 + paSalesTax))*100.0)/100.0; //total cost of pants plus tax rounded 
    System.out.println("The total cost of the pants is $" + totalCostOfPants);
    
    double totalCostOfShirts; //total cost of shirts
    totalCostOfShirts = Math.round(((shirtPrice * numShirts) * (1 + paSalesTax))*100.0)/100.0; //total cost of shirts plus tax rounded
    System.out.println("The total cost of the shirts is $" + totalCostOfShirts);
    
    double totalCostOfBelts; //total cost of belts
    totalCostOfBelts = Math.round(((beltPrice * numBelts) * (1 + paSalesTax))*100.0)/100.0;//total cost of belts plus tax rounded
    System.out.println("The total cost of the belt is $" + totalCostOfBelts); 
    
    double taxOnPants; //tax on pants
    taxOnPants = ((pantPrice * numPants) * (paSalesTax)); //price of pants multiplied by sales tax rounded
    System.out.println("The tax on the pants is $" + taxOnPants);
    
    double taxOnShirts; //tax on shirts
    taxOnShirts = (shirtPrice * numShirts) * (paSalesTax); //price of shirts multiplied by sales tax rounded
    System.out.println("The tax on the shirts is $" + taxOnShirts);
    
    double taxOnBelt; //tax on pants
    taxOnBelt = Math.round(((beltPrice * numBelts) * (paSalesTax))*100.0)/100.0; //price of belts multiplied by sales tax rounded
    System.out.println("The tax on the belt is $" + taxOnBelt);
    
    double totalCostBeforeTax;
    totalCostBeforeTax = Math.round(((totalCostOfPants + totalCostOfShirts + totalCostOfBelts) - (taxOnPants + taxOnShirts + taxOnBelt))*100.0)/100.0; //sum of cost without tax rounded
    System.out.println("The total spent on this purchase before tax is $" + totalCostBeforeTax);
    
    double totalTaxOnPurchases; //total tax on purchases
    totalTaxOnPurchases = Math.round((taxOnPants + taxOnShirts + taxOnBelt)*100.0)/100.0; //sum of tax on purchases rounded
    System.out.println("The total tax on this purchase is $" + totalTaxOnPurchases);
    
    double totalCostOfPurchases; //total cost of purchases
    totalCostOfPurchases = Math.round((totalCostOfPants + totalCostOfShirts + totalCostOfBelts)*100.0)/100.0; //sum of purchases rounded
    System.out.println("The total spent on this purchase after tax is $" + totalCostOfPurchases);
  }
}