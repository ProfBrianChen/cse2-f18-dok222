//Daniel Karkhut CSE 002

//This is a program that utilizes scanner to split a dinner check
//the inputs in the data are the original cost of the check, the desired tip percentage, and the number of ways the bill will be split
/////////
    import java.util.Scanner;
//
public class Check{
  
  public static void main(String[] args) {    
    Scanner myScanner = new Scanner (System.in); //enabling user input
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompt user for original check amount
    double checkCost = myScanner.nextDouble(); //Cost of check variable set to user input
    
    System.out.print("Enter the percentage tip that you wish to pay as a while number (in the from xx): "); //prompt user for tip percentage
    double tipPercent = myScanner.nextDouble(); //Percentage of tip is set to last scanner
    tipPercent /= 100; //convert percentage into decimal
    
    System.out.print("Enter the number of people who went out to dinner: "); //prompt for number of people
    int numPeople = myScanner.nextInt(); //Number of ways to split bill set to last input
    
    double totalCost; //Total Cost of Meal
    double costPerPerson; //Cost per person
    int dollars, //Whole dollar amounts of the cost
    dimes, pennies; //storing digits to the right of the decimal point for the cost
    totalCost = checkCost * (1 + tipPercent); //Cost of original check and tip
    costPerPerson = totalCost / numPeople; //Calculating cost per person
    dollars = (int) costPerPerson; //setting value to whole amount to drop decimal fractions
    dimes = (int) (costPerPerson * 10) % 10; //calculating dime amount
    pennies = (int) (costPerPerson * 100) % 10; //calculating penny amount
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //display total cost of dinner per person
    
  }

}